# Angular 2 Deep Dive

This is the source code accompanying the Angular 2 Deep Dive course.
This repository is based of [Antony Budianto's Angular 2 starter](https://github.com/antonybudianto/angular2-starter)

## Getting started

1. Clone the repository
2. Checkout out the tag corresponding to the video you are viewing, for the first video use `git checkout tags/s1v2`
3. Install dependencies: `npm install && bower install`
4. Run the server with `gulp`

Note that at the beginning of the Section 1 Video 2, the code will simply show an orange spinner in the browser.

